#!/bin/bash

# right now the solution is pretty ugly; consider parsing an XML file
# with some of the pointers given in this article: http://stackoverflow.com/questions/893585/how-to-parse-xml-in-bash

BIN_PATH=${HOME}/bin
BIN_FILE=${BIN_PATH}/getBratislavaWebcam.sh

# --- Nový Most ---

# variable arguments
CAM=3
VIEW=5
OUTPUT_PATH="/var/www/vhosts/phpgraphy/pictures/Bratislava"
OUTPUT_FILENAME="current.jpg"

$BIN_FILE -c $CAM -v $VIEW -p $OUTPUT_PATH -f $OUTPUT_FILENAME


# --- Starý Most ---
# CAM=5
# VIEW=1
# OUTPUT_PATH="/var/www/vhosts/phpgraphy/pictures/StaryMost"
# OUTPUT_FILENAME="current.jpg"

# $BIN_FILE -c $CAM -v $VIEW -p $OUTPUT_PATH -f $OUTPUT_FILENAME
