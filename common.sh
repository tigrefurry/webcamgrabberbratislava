print_action () {
    printf "%-50s" "$1"
}

print_status () {
    printf "%s\n" "$1"
}

# note that this expects a variable $DEBUG being set in the including script
print_debug () {
    if [ "$DEBUG" = true ]; then
        printf "[DEBUG %s] --- %s\n" "$(date '+%Y-%m-%d %H:%M:%S')" "$1" >&2
    fi
}

check_return_value () {
    rc=$?
    if [[ $rc != 0 ]] ; then
        print_status "failed!"
        exit $rc
    fi
}

