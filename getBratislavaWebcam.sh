#!/bin/bash

# ----------------------------------
#   INCLUDES
# ----------------------------------

source common.sh

# ----------------------------------
#   FUNCTIONS
# ----------------------------------

usage() {
    echo "Usage: $0 [-c <CAMERA_INDEX> [-v <VIEW INDEX>]] [-d <ALTERNATIVE_DATE>] [-p <OUTPUT_PATH>] [-f <OUTPUT_FILENAME>] [--debug] [--force]"

    exit 1
}

get_current_image_filename() {
    RESPONSE_STRING=$(curl --silent $CURL_ARGS)
    RESPONSE_ARRAY=(${RESPONSE_STRING//,/ })

    CURRENT_IMAGE=${RESPONSE_ARRAY[${#RESPONSE_ARRAY[@]}-1]}
}

check_temp_dir() {
    if [ ! -d $TMP_DIR ]
    then
        mkdir $TMP_DIR
    fi
}


# ----------------------------------
#   SETUP
# ----------------------------------

DEBUG=false
FORCE=false

BASE_URL=http://kamery.bratislava.sk

DATE=$(date '+%Y-%m-%d')
CAM=3
VIEW=5
OUTPUT_PATH=${HOME}
OUTPUT_FILENAME="current.jpg"

PATTERN='^\([[:digit:]]\{4\}-[[:digit:]]\{2\}-[[:digit:]]\{2\}_[[:digit:]]\{2\}-[[:digit:]]\{2\}\)\.jpg.*$'

TMP_DIR="/tmp/BratislavaWebcams"
check_temp_dir

# ----------------------------------
#   PARSE CMD LINE ARGS
# ----------------------------------

TEMP=`getopt -o c:v:d:p:f:h --long cam:,view:,date:,output-path:,output-filename,help,debug,force -n $0 -- "$@"`
eval set -- "$TEMP"

while true; do
    case "$1" in
        -h | --help)
            usage
            exit
            ;;

        -c | --cam)
            CAM="$2"; shift 2
            ;;

        -v | --view)
            VIEW="$2"; shift 2
            ;;

        -d | --date)
            DATE="$2"; shift 2
            ;;

        -p | --output-path)
            OUTPUT_PATH="$2"; shift 2
            ;;

        -f | --output-filename)
            OUTPUT_FILENAME="$2"; shift 2
            ;;

        --debug)
            DEBUG=true; shift
            ;;

        --force)
            FORCE=true; shift
            ;;

        --)
            shift
            break
            ;;

        *)
            echo "Unknown argument: $1"
            usage
            ;;
    esac
done

# http://kamery.bratislava.sk/get_photosofday.php?date=2014-09-06&cam=3&view=5
SERVICE_URL="${BASE_URL}/get_photosofday.php?date=${DATE}&cam=${CAM}&view=${VIEW}"
CURL_ARGS="$SERVICE_URL"

# http://ba-cam-canex.sk.data13.websupport.sk/cam3/2014-09-06/poz5/2014-09-06_18-45.jpg
IMAGE_URL="http://ba-cam-canex.sk.data13.websupport.sk/cam${CAM}/${DATE}/poz${VIEW}"

# --- DEBUG ---
print_debug "BASE_URL       : $BASE_URL"
print_debug "DATE           : $DATE"
print_debug "CAM            : $CAM"
print_debug "VIEW           : $VIEW"
print_debug "SERVICE_URL    : $SERVICE_URL"
print_debug "IMAGE_URL      : $IMAGE_URL"
print_debug "CURL_ARGS      : $CURL_ARGS"
print_debug "OUTPUT_PATH    : $OUTPUT_PATH"
print_debug "OUTPUT_FILENAME: $OUTPUT_FILENAME"
# -------------


# ----------------------------------
#   MAIN
# ----------------------------------

get_current_image_filename

if [ ! "$FORCE" = true ]
then
    if [ -f "${OUTPUT_PATH}/${CURRENT_IMAGE}" ]
    then
        echo "File ${OUTPUT_PATH}/${CURRENT_IMAGE} already exists. Skipping..."
        exit 0
    fi
fi

wget --directory-prefix $TMP_DIR "${IMAGE_URL}/${CURRENT_IMAGE}"
mv "${TMP_DIR}/${CURRENT_IMAGE}" $OUTPUT_PATH

SNAPSHOT_TIMESTAMP=$(echo $CURRENT_IMAGE | sed "s/$PATTERN/\1/")
print_debug "SNAPSHOT_TIMESTAMP: $SNAPSHOT_TIMESTAMP"

CURRENT_TIMESTAMP=$(date "+%Y-%m-%d %H:%M:%S")
print_debug "CURRENT_TIMESTAMP: $CURRENT_TIMESTAMP"

MONTAGE_LABEL="Image taken: $SNAPSHOT_TIMESTAMP --- Fetched: $CURRENT_TIMESTAMP"
print_debug "MONTAGE_LABEL: $MONTAGE_LABEL"

montage -label "$MONTAGE_LABEL" "${OUTPUT_PATH}/$CURRENT_IMAGE" -geometry +0-0 -background Plum ${OUTPUT_PATH}/$OUTPUT_FILENAME
